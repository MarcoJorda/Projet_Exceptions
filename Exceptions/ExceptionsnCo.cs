﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    class ExceptionsnCo
    {
        public static double RacineCarree(double valeur)
        {
            if (valeur <= 0)
                throw new ArgumentOutOfRangeException("valeur", "Le paramètre doit être positif Bosetti!");
            return Math.Sqrt(valeur);
        }

        public static void Exceptions_du_Futur(string[] args)
        {
            double alexis;

            alexis = RacineCarree(42);
            Console.WriteLine(alexis);
            Console.ReadKey();

            try
            {
                string user_value = "20";
                int value = int.Parse(user_value);
                Console.WriteLine("This line has been executed today but absolutely anybody cares.");
                Console.ReadKey();
            }
            catch (Exception)
            {
                Console.WriteLine("Error during user value conversion.");
                Console.ReadKey();
            }
        }
    }
}
