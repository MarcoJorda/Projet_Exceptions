﻿using System.Collections.Generic;

namespace MyMath
{
    public class MultipleDivision
    {
        private List<int> values;
        public MultipleDivision(List<int> values)
        {
            this.values = values;
        }

        public double Division(int index_dividende, int index_diviseur)
        {
            double quotient;
            quotient = (double)this.values[index_dividende] / (double)this.values[index_diviseur];               
            return quotient;
        }
    }
}
