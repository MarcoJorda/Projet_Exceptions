﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMath;


namespace Exceptions
{
    class Program
    {      

        static void Main(string[] args)
        {
            
            List<int> values = new List<int> { 17, 12, 15, 38, 29, 157, 89, -22, 0, 5 };
            MultipleDivision mltd = new MultipleDivision(values);

            int index_dividende, index_diviseur;

            Console.Write("Veuillez écrire votre index_dividende : ");
            index_dividende = (int.Parse(Console.ReadLine()));

            Console.Write("Veuillez écrire votre index_diviseur : ");
            index_diviseur = (int.Parse(Console.ReadLine()));

            double quotient = mltd.Division(index_dividende, index_diviseur);
            Console.WriteLine(quotient);

            Console.ReadKey();
        }
        
    }
}
